
import math

from datetime import datetime
from flask import Flask, request
from flask_cors import CORS

app = Flask(__name__)
CORS(app)

user_availability = True
current_cab_location = [5, 5]

def update_cab_location(cab_location):

    current_cab_location =cab_location

    return current_cab_location



#  function to validate register cab driver request data
def validate_register_driver_data(data):
    if not data.get('name'):
        print('name is missing')
        return {'error': 'name is missing'}
    if not data.get('licence_no'):
        print('licence_no is missing')
        return {'error': 'licence_no is missing'}
    if not data.get('address'):
        print('address is missing')
        return {'error': 'address is missing'}
    if not data.get('identification_no'):
        print('identification_no is missing')
        return {'error': 'identification_no is missing'}

    return


# function to validate register cab booking request data
def validate_register_booking_data(data):
    if not data.get('pickup_loc'):
        return {'error': 'pickup location not found'}
    if not data.get('destination_loc'):
        return {'error': 'destination_loc not found'}
    if not data.get('user_name'):
        return {'error': 'user_name not found'}
    if not data.get('mobile'):
        return {'error': 'mobile no not found'}

    return

# Route to register the driver in the system
@app.route('/register-driver', methods=['POST'])
def register_driver(request_data):
    validation_error_response = validate_register_driver_data(request_data)
    if validation_error_response:
        # returning error code 400 with message if there is any validation error
        print('Validation Error', validation_error_response.get('error'))
        return validation_error_response, 400

    # response = _make_database_entry(request_data)
    return {
        'message': 'Registered done Successfully'
    }, 200

# Route to register a booking with given location coordinates
@app.route('/register-booking', methods=['POST'])
def register_booking(request_data):
    validation_error_response = validate_register_booking_data(request_data)
    if validation_error_response:
        # returning error code 400 with message if there is any validation error
        print('Validation Error', validation_error_response.get('error'))
        return validation_error_response, 400

    pickup_point = request_data.get('pickup_loc') #[3,5]
    destination = request_data.get('destination_loc')   #[7,8]
    cab_location = current_cab_location
    # formula to be used √((x2-x1)2+(y2-y1)2)
    # Calculating the distance
    distance = math.sqrt((destination[1] - cab_location[1])^2 + (destination[0] - cab_location[0])^2)

    # response = _make_database_entry(request_data, distance)

    return{
        'message': 'registered booking',
        'distance': distance
    }, 200



# Route to modify the availability of driver and update cab location
@app.route('/modify-availability', methods=['POST'])
def modify_availability(request_data):
    validation_error_response = validate_modify_availability_data(request_data)
    if validation_error_response:
        # returning error code 400 with message if there is any validation error
        print('Validation Error', validation_error_response.get('error'))
        return validation_error_response, 400

    user_availability = request_data.get('user_availability')

    # if user is sending True, updating the cab location to new location
    if user_availability == True:
        update_cab_location(request_data.get('new_location'))

    return {
        'message': 'Availability Updated Successfully'
    }, 200

